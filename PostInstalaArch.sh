#!/bin/bash

echo Script Postinstalación ArchLinux realizado por Juan Febles - Podcast Linux

sleep 1
echo Instalar primeros paquetes
sudo pacman -S telegram-desktop firefox neofetch p7zip --needed --noconfirm

sleep 1
echo Instalar Temas para globales para KDE
wget -qO- https://raw.githubusercontent.com/PapirusDevelopmentTeam/arc-kde/master/install.sh | sh
wget -qO- https://git.io/papirus-icon-theme-install | DESTDIR="$HOME/.local/share/icons" sh
sudo pacman -S arc-gtk-theme --needed --noconfirm

sleep 1
echo Instalar paquetes y aplicaciones KDE
sudo pacman -S qt5-xmlpatterns spectacle okular kid3 partitionmanager --needed --noconfirm

sleep 1
echo Instalar paquetes y aplicaciones Multimedia
sudo pacman -S qpwgraph calf kdenlive audacity ardour inkscape gimp simplescreenrecorder libreoffice-fresh-es hunspell-es_es obs-studio godot hugo --needed --noconfirm

sleep 1
echo Instalar paquetes para el Kernel
sudo pacman -S base-devel linux-zen-headers --needed --noconfirm

sleep 1
echo Instalar paquete ia para archive.org
sudo pacman -Sy python-pip --needed --noconfirm
sudo pip install internetarchive
ia configure

sleep 1
echo Instalar Joplin
wget -O - https://raw.githubusercontent.com/laurent22/joplin/dev/Joplin_install_and_update.sh | bash
sudo pacman -S fuse --needed --noconfirm

sleep 1
echo Credenciales ssh Gitlab. Recuerda copiar tu carpeta .ssh dentro de tu Home
sleep 2
ssh -T git@gitlab.com
echo Introduce tu usuario Gitlab
read usuario
git config --global user.name "$usuario"
echo Introduce tu correo Gitlab
read correo
git config --global user.email "$correo"
git config --global --list

sleep 1
echo Activar Repo Chaotic-AUR
sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
sudo pacman-key --lsign-key FBA220DFC880C036
sudo pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
sudo sed -i '$ a \\n[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist' /etc/pacman.conf

sleep 1
echo Configurar pacman.conf
sudo sed -i -e "s/^#ParallelDownloads = 5$/ParallelDownloads = 5/" /etc/pacman.conf
sudo sed -i -e "s/^#Color$/Color/" /etc/pacman.conf
sudo sed -i -e"s/^#VerbosePkgLists$/VerbosePkgLists/" /etc/pacman.conf
sudo sed -i "/ParallelDownloads = 5/a ILoveCandy" /etc/pacman.conf

sleep 1
echo Yay y Octopi
sudo pacman -S --needed --noconfirm git base-devel && git clone https://aur.archlinux.org/yay.git && cd yay && makepkg -si
cd
yay -S octopi --noconfirm
sudo pacman -Sy octopi-notifier-qt5 --needed --noconfirm

sleep 1
echo Activar Reflector
sudo pacman -S reflector  --needed --noconfirm
sudo cp -vf /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
sudo reflector --verbose -l 5 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

sleep 1
echo Clonar repositorio Gitlab podcastlinux.com
cd ~/Documentos
git clone git@gitlab.com:podcastlinux/podcastlinux.gitlab.io.git

sleep 1
echo Instalar Dark Theme para Grub
git clone --depth 1 https://gitlab.com/VandalByte/darkmatter-grub-theme.git && cd darkmatter-grub-theme
sudo python3 darkmatter-theme.py --install
cd
sleep 1
echo "¿Quieres instalar tlp para gestionar mejor la batería?"
read tlp
if [[ "$tlp" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] ;then
sudo pacman -S tlp --noconfirm
systemctl enable tlp.service --noconfirm
systemctl mask systemd-rfkill.service systemd-rfkill.socket
sudo tlp-stat -s
fi

sleep 1
echo "¿Deseas instalar impresoras en este ordenador?"
read printer
if [[ "$printer" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] ;then
sudo pacman -S cups ghostscript gsfonts libcups --noconfirm
sudo systemctl enable cups.service
sudo systemctl start cups.service
echo Recuerda que debes administrarlas en http://localhost:631/admin
echo Los drivers búscalos en https://www.openprinting.org
fi

sleep 1
echo "¿Deseas activar la huella dactilar en este ordenador?"
read huella
if [[ "$huella" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] ;then
sudo pacman -S fprintd --noconfirm
fprintd-enroll
sudo sed -i '1 a \\nauth       sufficient    pam_unix.so try_first_pass likeauth nullok\nauth       sufficient    pam_fprintd.so' /etc/pam.d/system-local-login
sudo sed -i '1 a \\nauth       sufficient    pam_unix.so try_first_pass likeauth nullok\nauth       sufficient    pam_fprintd.so' /etc/pam.d/sddm
sudo sed -i '1 a \\nauth       sufficient    pam_unix.so try_first_pass likeauth nullok\nauth       sufficient    pam_fprintd.so' /etc/pam.d/kde
sudo sed -i '1 a \\nauth       sufficient    pam_unix.so try_first_pass likeauth nullok\nauth       sufficient    pam_fprintd.so' /etc/pam.d/sudo
sudo pacman -S kwalletmanager --noconfirm
fi


sleep 1
echo "¿Deseas activar el Bluetooth en este ordenador?"
read bluetooth
if [[ "$bluetooth" =~ ^(SI|si|S|s|y|yes|Y|YES)$ ]] ;then
sudo pacman -S bluez bluez-utils blueman pipewire-pulse --noconfirm
sudo systemctl start bluetooth.service
sudo systemctl enable bluetooth.service
sudo touch /etc/pulse/default.pa
sudo sed -i '1 a \\n### Automatically switch to newly-connected devices\nload-module module-switch-on-connect' /etc/pulse/default.pa
fi
exit
