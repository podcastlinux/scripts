#!/bin/bash

echo -e "¿(p)odcast Linux o (l)inux Express?: "
read podcast
if [[ $podcast == "p" ]]
then
echo -e "Episodio a convertir: "
read episodio
num=$(($episodio + 1))
echo -e "Título episodio: "
read titulo

cd ~/Música/PL$episodio/export/
ffmpeg -y -i PL$episodio.flac -i ~/Imágenes/PL$episodio.png -map_metadata 0 -map 0 -map 1 -metadata title="#$episodio $titulo" -metadata artist="Podcast Linux" -metadata genre="Podcast" -metadata track="$num" -metadata copyright="CC BY - SA 4.0" -metadata year="2020" -metadata comment="Podcast realizado por Juan Febles con Ardour" -ar 44100 -ac 1 -ab 80k PL$episodio.mp3 
ffmpeg -y -i PL$episodio.flac -i ~/Imágenes/PL$episodio.png -map_metadata 0 -map 0 -map 1 -metadata title="#$episodio $titulo" -metadata artist="Podcast Linux" -metadata genre="Podcast" -metadata track="$num" -metadata copyright="CC BY - SA 4.0" -metadata year="2020" -metadata comment="Podcast realizado por Juan Febles con Ardour" -c:a libvorbis -ar 44100 -ac 1 -ab 80k PL$episodio.ogg
olength=$(wc -c PL$episodio.ogg | awk '{print $1}')
mlength=$(wc -c PL$episodio.mp3 | awk '{print $1}')
iduration=$(ffmpeg -i PL$episodio.ogg 2>&1 | grep Duration| cut -c 13-20)
cd ~/Documentos/podcastlinux.gitlab.io/content/posts/podcastlinux
sed -i "s/olength :/olength : $olength/g" $episodio-Podcast-Linux.md
sed -i "s/mlength :/mlength : $mlength/g" $episodio-Podcast-Linux.md
sed -i "s/iduration :/iduration : \"$iduration\"/g" $episodio-Podcast-Linux.md

#ia upload podcast_linux PL$episodio.mp3 PL$episodio.ogg
elif [[ $podcast == "l" ]]
then
echo -e "Episodio a convertir: "
read episodio

cd ~/Música/
ffmpeg -y -i "$episodio"linuxexpress.flac -i ~/Imágenes/linuxexpress.png -map_metadata 0 -map 0 -map 1 -metadata title="#$episodio Linux Express" -metadata artist="Podcast Linux" -metadata genre="Podcast" -metadata track="$episodio" -metadata copyright="CC BY - SA 4.0" -metadata year="2020" -metadata comment="Podcast realizado por Juan Febles con Audacity" -ar 44100 -ac 1 -ab 64k ~/Documentos/podcastlinux.gitlab.io/static/Linux-Express/"$episodio"linuxexpress.mp3 
ffmpeg -y -i "$episodio"linuxexpress.flac -i ~/Imágenes/linuxexpress.png -map_metadata 0 -map 0 -map 1 -metadata title="#$episodio Linux Express" -metadata artist="Podcast Linux" -metadata genre="Podcast" -metadata track="$episodio" -metadata copyright="CC BY - SA 4.0" -metadata year="2020" -metadata comment="Podcast realizado por Juan Febles con Audacity" -c:a libvorbis -ar 44100 -ac 1 -ab 64k ~/Documentos/podcastlinux.gitlab.io/static/Linux-Express/"$episodio"linuxexpress.ogg    
cd ~/Documentos/podcastlinux.gitlab.io/static/Linux-Express
olength=$(wc -c "$episodio"linuxexpress.ogg | awk '{print $1}')
mlength=$(wc -c "$episodio"linuxexpress.mp3 | awk '{print $1}')
iduration=$(ffmpeg -i "$episodio"linuxexpress.ogg 2>&1 | grep Duration| cut -c 13-20)
cd ~/Documentos/podcastlinux.gitlab.io/content/posts/linuxexpress
sed -i "s/olength :/olength : $olength/g" $episodio-Linux-Express.md
sed -i "s/mlength :/mlength : $mlength/g" $episodio-Linux-Express.md
sed -i "s/iduration :/iduration : \"$iduration\"/g" $episodio-Linux-Express.md
else
echo "Debes elegir un Podcast"
fi




